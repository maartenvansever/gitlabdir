Table Booking as B {
  id int [pk]
  distance int
  refuel_costs decimal
  user string [ref: > U.shareholder]
  vehicle id [ref: > V.id]
}

Table Person as P {
    rrnr string [pk]
    name string
}

Table ShareHolder as SH {
  person string [pk, ref: > P.rrnr]
  shares int
}

Table User as U {
  shareholder string [pk, ref: > SH.person]
}

Table Provider {
  shareholder string [pk, ref: > SH.person]
}

Table VehicleTester {
  shareholder string [pk, ref: > SH.person]
}

Table Admin {
  shareholder string [pk, ref: > SH.person]
}

Table Vehicle as V {
  id int [pk]
  policy int [ref: > Policy.id]
  owner string [ref: > Provider.shareholder]
  name string
  stationedAt string
  description blob
}

Table Car {
  vehicle int [pk, ref: > V.id]
  category int [ref: > CarCategory.id]
}

Table CarCategory {
  id int [pk]
}

Table Bike {
  vehicle int [pk, ref: > V.id]
  category int [ref: > BikeCategory.id]
}

Table BikeCategory {
  id int [pk]
}

Table Policy as P {
  id int [pk]
}
Table Test{
 id int[pk, increment, unique]
 rating int
 date date
 test_report string
 vehicleId int [ref: > Vehicle.id]
 vehicletester string [ref: > VehicleTester.shareholder]
 
  }

