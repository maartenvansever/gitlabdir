#Information Systems

## Oefeningenreeks: Externe HTTP APIs consumeren

O. Sourie, K. Roels / Howest - Toegepaste Informatica


##Algemeen
Voor elke request dien je Postman/woman te gebruiken, tenzij anders vermeld.<br/>
Maak ook steeds een screenshot van elke handeling/aanvraag als deel van je antwoord.



##Deel 1
<b>API</b>  https://reqres.in/api

###Oefening 1

1. Welke entiteit, naast ``users``, kan ook opgevraagd worden in deze API?
2. Wat is de URL hiervan?
3. Vraag en download alle data hiervan op 
4. Hoeveel instanties zijn er in totaal van deze entiteit? Waaruit kun je dit specifiek afleiden?
5. Geef de request een naam en voeg deze toe aan je bestaande collectie **ReqRes API**

###Oefening2

Maak een JavaScript toepassing die de gegevens van alle ``users`` toont, inclusief foto.<br/>
Let op: alle data dient in één ononderbroken lijst te staan.

![Oefeningenreeks13-APIConsuming-Deel1-Oefening](C:\Users\olivi\Hogeschool West-Vlaanderen\TI - Curriculummateriaal\S3 - Information Systems (AdvSQL-InformExch-NoSQL)\02 Oefeningen\Oefeningenreeks13-APIConsuming-Deel1-Oefening2.PNG)

<div style="page-break-after: always; break-after: page;"></div>  
##Deel 2
<b>API</b>  https://www.metaweather.com/api/

###Oefening 1

1. Toon de locatie-informatie van de steden waarvan de tekenreeks ``bu`` in de naam voorkomt.
2. Vraag de weerinformatie op van vandaag en de volgende 5 dagen voor ``Brussel``.
3. Toon de beschikbare weerinformatie op van de stad ``Amsterdam`` op 23/11/2019.
4. Bewaar al deze requests in de nieuwe collectie **MetaWeather API**

###Oefening2

Maak voor oefening 1.2 nu ook een JavaScript toepassing die volgende specifieke informatie toont:

- De dagen van de week
- De weerstoestand aan de hand van een gepaste afbeelding voor elk van de dagen 
  (vb:  /static/img/weather/png/64/showers.png)
- De minimum- en maximumtemperatuur voor elke dag





















<div style="page-break-after: always; break-after: page;"></div>  
##Deel 3

<b>API</b>   https://swapi.co/api/ (Star Wars API)

###Oefening 1

1. Vraag de lijst op van alle beschikbare entiteiten (end points)
2. Toon respectievelijk alle informatie van personages, films en soorten
3. Toon de 4e pagina van de lijst van planeten
4. Toon alle info over het voertuig ``Sith speeder``
5. Toon alle voertuigen van het type ``speeder``
6. Toon de informatie van het ruimteschip ``Star Destroyer`` in ***Wookiee*** formaat
7. Bewaar al deze requests in de nieuwe collectie **Star Wars API**
8. Hoeveel ruimteschepen zijn er aanwezig? Waaruit kun je dit afleiden?

###Oefening2

Maak een JavaScript toepassing die de naam, thuiswereld en soort van alle personages toont in een lijstvorm. Bij het klikken op een naam verschijnen de filmtitels waarin dit personage speelt.

Hou rekening met de paginering: voorzie de nodige navigatie naar vorige en/of volgende pagina.






<div style="page-break-after: always; break-after: page;"></div>  
##Deel 4
<b>API</b>  https://api.openbrewerydb.org/

###Oefening 1

1. Vraag de lijst op van alle geregistreerde brouwerijen
2. Toon enkel id en naam van alle brouwerijen waarbij de tekst ``brewing`` voorkomt in de naam
3. Toon alle data van de brouwerijen van het type ``contract``
4. Toon alle info van de brouwerijen van de stad ``Birmingham`` in de staat ``Alabama``
5. Toon alle data van de brouwerijen uit de staat ``Arizona`` gesorteerd op stad en brouwerijtype
6. Toon de tweede pagina uit de lijst van alle brouwerijen uit ``Anchorage`` (elke pagina toont slechts 5 brouwerijen)
7. Bewaar al deze requests in de nieuwe collectie **Brewery API**

###Oefening2

Maak een JavaScript toepassing die een dropdownlist toont met alle geregistreerde staten in de VS.<br/>
Bij het aanklikken van een staat verschijnt hiervoor een lijst van alle brouwerijnamen, hun website (hyperlink) en de stad waarin ze aanwezig zijn. <br/>
Toon ook steeds het totaal aantal gevonden items.

Het aantal effectief getoonde brouwerijen is afhankelijk van de waarde die uit een extra invoerveld kan gelezen worden.







