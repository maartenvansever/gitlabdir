#Information Systems

## Oefeningenreeks: GraphQL Query/Mutations

O. Sourie, K. Roels / Howest - Toegepaste Informatica


##Algemeen
Voor elke request kun je gebruik maken van Postman/-woman/GraphiQL.<br/>
Maak ook steeds een screenshot van elke handeling/aanvraag als deel van je antwoord.



##Deel 1
<b>GraphQL API</b>  https://api.graphql.jobs/

1. Toon van alle beschikbare jobs de titel, beschrijving, bedrijfsnaam, Url van het bedrijf en opdrachtpercentage (full-time, ...)
  
   > 
   
2. Toon van elk bedrijf de naam en de gepubliceerde jobs<br/>Van elke job zien we de titel, URL waarop kan gesolliciteerd worden en wanneer deze geplaatst is

   > 
   
3. Toon van elk land de naam en de gepubliceerde jobs<br/>Van elke job zien we de titel en in welke stad deze beschikbaar is

  >  
  
4. Zie vraag 3, maar toon nu enkel de jobs waarbij de titel de tekst ``JavaScript `` bevat

  > 
  
5. Toon enkel de naam van de stad voor jobs uit ``Berlijn``

  > 
  
6. Toon voor elke stad de naam van het land en de jobs waarbij de titel het begrip ``Backend`` bevat<br/>
  Sorteer daarbij de jobs in dalende volgorde op creatiedatum<br/>
Geef de naam ``CitiesWithJobs`` aan deze query
  
  >

<div style="page-break-after: always; break-after: page;"></div>  
##Deel 2
<b>GraphQL API</b>  https://countries.trevorblades.com/

1. Toon alle beschikbare talen
  
  > 
  
2. Toon voor elk land de Engelstalige en inlandse naam

  > 
  
3. Toon voor alle landen van alle continenten de naam, munteenheid en de officiële talen.

  > 
  
4. Toon voor elk land de code, naam, telefoonprefix, naam van het continent en de gesproken talen

   >  
   
   





















<div style="page-break-after: always; break-after: page;"></div>  
##Deel 3

<b>GraphQL API</b>   https://swapi.graph.cool/ (Star Wars API)<br/>
Let op: Geef nu telkens ook een naam aan je query

1. Vraag de titel en uitgavedatum van de eerste 5 beschikbare films op, gesorteerd op titel
  
  > 
  
2. Toon de titel en regisseur van de film met de titel ``A New Hope``
  
  > 
  
3. Toon voor elke film de episode, titel, producers, spelers (naam en geslacht) en het totaal aantal spelers
  
  > 
  
4. Toon voor alle planeten zoveel mogelijk geografische data (klimaat, diameter, rotatieperiode, enz.) alsook het aantal films waarin deze is voorgekomen
  
  > 
  
5. Vraag alle planeten op die langer rond hun ster draaien en kortere dagen hebben dan onze planeet<br/>Hun diameter dient ook groter te zijn dan die van de aarde<br/>
***Fancy Nancy***: werk dit uit met variabelen
  
  > 
  
6. Toon alle technische info over het voertuig ``Sith speeder``

  >  
  
7. Vraag van alle ruimteschepen de naam, hyperdrive rating, capaciteit en de piloten (naam, taal en gemiddelde levensverwachting) met een hyperdrive rating groter of gelijk aan 1<br/>De piloten worden enkel getoond als ze van de soort ``mens`` of ``wookiee`` zijn

  > 
  
8. Hoeveel ruimteschepen zijn er aanwezig met een hyperdrive rating van 4 of meer en een capaciteit van meer dan 40000?

  > 
  
9. Hoeveel films bestaan er waarbij elke figurerende planeet een diameter van meer dan 9000 miles heeft?<br/>
  ***Fancy Nancy***: werk met variabelen

  >    
  
10. Hoeveel planeten, ruimteschepen en soorten zijn er aanwezig?

  > 


<div style="page-break-after: always; break-after: page;"></div>  
##Deel 4
<b>GraphQL API</b>  https://todo-mongo-graphql-server.herokuapp.com/<br/>
Let op: Geef nu telkens ook een naam aan je query/mutation

1. Vraag voor alle todos de id, beschrijving op en of deze afgewerkt zijn 
  
  >  
  
2. Voeg een eigen onafgewerkte todo toe
  
  >   
  
3. Verander de status hiervan nu in ``completed`` 
  
   > 
   
4. Pas de tekst van je eigen todo aan en markeer deze als niet afgewerkt 

  > 
  
5. Verwijder tenslotte deze todo
  
  > 





