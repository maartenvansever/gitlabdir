#Information Systems

## Oefeningenreeks: Eigen JSON Server

O. Sourie, K. Roels / Howest - Toegepaste Informatica


##Algemeen

Maak steeds een screenshot van elke handeling/aanvraag als deel van je antwoord.



###Oefening 1 - JSON data

1. Zet de tabel ``auteurs`` uit de database ``MijnBoeken`` om naar JSON-formaat vanuit SQL Server

2. Plaats deze vervolgens in het bestand ``mijnboeken.json`` in de map van je JSON server

3. Voorzie de nodige configuratie in ``package.json``  en start de boekenserver op
  
4. Ga na welke end points je nu kunt gebruiken
  
5. Zet vervolgens de entiteiten ``boeken``, ``boekenauteurs`` en ``uitgevers`` uit de database ``MijnBoeken`` om naar JSON-formaat en plaats deze in ``mijnboeken.json`` 

6. Welke end points zijn beschikbaar?

<div style="page-break-after: always; break-after: page;"></div>  
###Oefening 2 - GET / POST / PUT / DELETE

Maak terug gebruik van Postman/woman om de end points van je eigen API te testen.<br/>
Stel ook steeds maar één request per vraag op, tenzij anders vermeld.<br/>

1. Toon alle ``auteurs`` uit San Francisco, gesorteerd op familienaam

2. Voeg deze nieuwe data toe:

   > Auteur: Cyriel Buysse, Nevele, 20/9/1859<br/>
   >
   > Boek:  Twee werelden (1931), Roman, uitgeverij Lannoo

3. Toon de eerste 5 ``uitgevers`` 

4. Ga na welke ``uitgevers`` er achter de id's 2 en 3 schuilgaan (gebruik één request)

5. Toon de ``boeken`` waarvan de prijs tussen 19 en 40 euro ligt.

6. Verander de geboortedatum van Herta Müller in 6 juli 1961

7. Toon nu alle ``auteurs`` die geboren zijn in 1961

8. Toon voor elk boek de info van dat boek alsook alle info van de bijhorende uitgeverij

9. Toon alle ``boeken`` van de uitgeverij met id 4.

10. Toon alle ``boeken`` van de uitgeverij met id 2 inclusief alle info over de uitgeverij zelf

11. Toon de algemene info van uitgeverij ``De Bezige Bij`` met daarbij de lijst van hun uitgegeven ``boeken`` 




<div style="page-break-after: always; break-after: page;"></div>  