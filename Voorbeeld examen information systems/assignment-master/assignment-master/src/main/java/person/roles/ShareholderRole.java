package person.roles;

import person.Shareholder;

import java.util.Objects;

public abstract class ShareholderRole {

    private final Shareholder shareholder;

    ShareholderRole(Shareholder shareholder) {
        this.shareholder = Objects.requireNonNull(shareholder);
    }
}
