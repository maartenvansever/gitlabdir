package person.roles;

import booking.Booking;
import person.Shareholder;

import java.util.HashSet;
import java.util.Set;

public class User extends ShareholderRole {

    private final Set<Booking> bookings = new HashSet<>();

    public User(Shareholder shareholder) {
        super(shareholder);
    }

    public void addBooking(Booking booking) {
        this.bookings.add(booking);
        booking.addUser(this);
    }
}
