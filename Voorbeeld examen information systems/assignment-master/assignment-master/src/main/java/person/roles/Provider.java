package person.roles;

import person.Shareholder;
import vehicle.Vehicle;

import java.util.HashSet;
import java.util.Set;

public class Provider extends ShareholderRole {

    private final Set<Vehicle> vehicles = new HashSet<>();

    public Provider(Shareholder shareholder, Set<Vehicle> vehicles) {
        super(shareholder);
        // TODO: add vehicle owner logic and add vehicles
    }

    public void addVehicle(Vehicle vehicle) {
        // TODO: add vehicle logic
    }

    public Set<Vehicle> getVehicles() {
        return vehicles;
    }
}
