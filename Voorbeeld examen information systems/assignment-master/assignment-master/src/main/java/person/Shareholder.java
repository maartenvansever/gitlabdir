package person;

import person.roles.ShareholderRole;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class Shareholder extends Person {

    private final int shares;
    private final Set<ShareholderRole> roles;

    public Shareholder(int RRNR, String name, int shares, Set<Function<Shareholder,ShareholderRole>> roleSuppliers)
        throws InstantiationException {
        super(RRNR, name);
        if(Objects.isNull(roleSuppliers) || roleSuppliers.stream().allMatch(Objects::isNull)) {
            throw new InstantiationException("You must provide at least one role");
        }
        this.shares = shares;
        this.roles = roleSuppliers.stream()
                         .map(supplier -> supplier.apply(this))
                         .collect(Collectors.toSet());
    }

    public int getShares() {
        return shares;
    }

    public Set<ShareholderRole> getRoles() {
        return roles;
    }
}
