package person;

import java.util.Objects;

public class Person {
    private final int RRNR;
    private final String name;

    Person(int RRNR, String name) {
        Objects.requireNonNull(name);
        this.RRNR = RRNR;
        this.name = name;
    }

    public int getRRNR() {
        return RRNR;
    }

    public String getName() {
        return name;
    }

}
