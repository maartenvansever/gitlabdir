package person.groups;

import person.Shareholder;
import person.roles.ShareholderRole;

import java.util.Set;
import java.util.function.Function;

public class Participant extends Shareholder {
    public Participant(int RRNR, String name, int shares,
                        Set<Function<Shareholder, ShareholderRole>> roleSuppliers)
        throws InstantiationException {
        super(RRNR, name, shares, roleSuppliers);
    }
}
