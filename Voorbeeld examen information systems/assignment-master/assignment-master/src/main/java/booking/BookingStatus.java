package booking;

import java.time.LocalDate;
import java.util.Objects;

public class BookingStatus {
    private final Booking booking;
    private final LocalDate date;
    private final BookingStatusEnum status;

    public BookingStatus(Booking booking, LocalDate date, BookingStatusEnum status) {
        this.booking = Objects.requireNonNull(booking);
        this.date = Objects.requireNonNull(date);
        this.status = Objects.requireNonNull(status);
    }

    public LocalDate getDate() {
        return date;
    }

    public BookingStatusEnum getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookingStatus)) return false;
        BookingStatus that = (BookingStatus) o;
        return booking.equals(that.booking) &&
                   date.equals(that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(booking, date);
    }
}
