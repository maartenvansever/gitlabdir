package booking;

import person.roles.User;
import vehicle.Vehicle;

import java.math.BigDecimal;
import java.time.Period;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class Booking {
    private final UUID id;

    public Booking(Vehicle vehicle, User user, Period period) {
        this.id = UUID.randomUUID();
   }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Booking)) return false;
        Booking booking = (Booking) o;
        return id.equals(booking.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
