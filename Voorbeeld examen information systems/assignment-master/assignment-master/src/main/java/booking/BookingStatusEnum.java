package booking;

public enum BookingStatusEnum {
    RESERVED, CANCELLED, RENTED, RETURNED, PAYED;
}
