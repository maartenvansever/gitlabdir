package vehicle;

import person.roles.VehicleTester;

import java.time.LocalDate;
import java.util.Objects;

public class VehicleTest {
    private final int rating;
    private final LocalDate date;
    private final String report;
    private final Vehicle vehicle;
    private VehicleTester tester;

    public VehicleTest(int rating, String report, LocalDate date, VehicleTester tester, Vehicle vehicle) {
        // TODO: null checks, add test logic
        this.tester = tester;
        this.vehicle = vehicle;
        this.date = date;
        this.rating = rating;
        this.report = report;
    }

    public void setTester(VehicleTester tester) {
        // TODO: set tester logic
    }

    public int getRating() {
        return rating;
    }

    public String getReport() {
        return report;
    }

    public LocalDate getDate() {
        return date;
    }

    public VehicleTester getTester() {
        return tester;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }
}
