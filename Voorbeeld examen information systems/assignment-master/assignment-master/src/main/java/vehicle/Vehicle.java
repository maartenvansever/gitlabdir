package vehicle;

import booking.Booking;
import person.groups.Participant;
import person.roles.Provider;
import util.Location;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Vehicle {

    private final UUID id;
    private final String name;
    private final Location stationedAt;
    private final String description;
    private Set<VehicleTest> tests = new HashSet<>();
    private Provider owner;

    public Vehicle(UUID id, String name, Location stationedAt, String description) {
        this.id = id;
        this.name = name;
        this.stationedAt = stationedAt;
        this.description = description;
    }

    public void addTest(VehicleTest test)  {
        // TODO: add test logic
    }

    public BigDecimal calcCost(Booking booking) {
        return BigDecimal.ZERO ;
    }

    public void setOwner(Provider owner) {
        // TODO: add vehicle owner logic
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Location getStationedAt() {
        return stationedAt;
    }

    public String getDescription() {
        return description;
    }

    public Set<VehicleTest> getTests() {
        return tests;
    }

    public Provider getOwner() {
        return owner;
    }
}
