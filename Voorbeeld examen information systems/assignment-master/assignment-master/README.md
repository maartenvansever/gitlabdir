# Vehicle Sharing

This is a Vehicle Sharing application. The system works like this:

* Any car owner (Provider) can propose to add it's vehicles to the pool of vehicles of the system. A vehicle is added after it has been tested by one or more testers. During a test, a small report is added, as well as a grading.
* Any user (User) can book and use the vehicles for a certain period of time (Rental Period). 
* Some vehicles, such as cars and speedpedelec must have a license plate, while others don't have one. 
* All vehicles must have an insurance, and an insurance company can provide a policy on which several vehicles are covered, having some specific conditions.
* Everyone who uses or provides a vehicle, must be a shareholder. Also all admins and testers are shareholders.
* Very vehicle has it's own calendar on which one can make a booking (via the Rental Period) - of course, in the client, all the calendars can be shown together.
* For every booking, the cost is calculated. This cost is what has to be payed by the user. The cost consists of the vehicle cost and the fines on that vehicle during that period it was rented, subtracting the refuel costs made by the user.
* The vehicle cost depends on the vehicle category:
  - For every car, the cost per km is calculated based on depreciation, expected km per year, ... . The actual cost per rental period is the distance driven during that period multiplied by the cost/km.
  - For bikes, there is a fixed price for each bike category. Bikes can be classic bikes, electrical bikes, cargo bikes and even electrical cargo bikes. There may be other bikes in the future. Electrical cargo speedpedelecs also exist. 

# Project structure

* **asyncapi**: example of an asyncapi spec, to be validated against https://playground.asyncapi.io/; spec: https://www.asyncapi.com/docs/specifications/2.0.0/
* **datomic schema**: example of a datomic schema; spec: https://docs.datomic.com/on-prem/schema.html 
* **dbdiagram**: an unfinished DB model, to be validated against https://dbdiagram.io
* **gradle/wrapper**: gradle directory, no need to so something here
* **information model**: an ER diagram made in [TerraER.jar](http://www.terraer.com.br/). Please make sure you enable model checking, so your model is valid!
* **jsonschema** : example of a json schema, to be validated against https://www.jsonschemavalidator.net/ ; spec:  https://json-schema.org/draft/2019-09/json-schema-validation.html
* **openapi**: example of an openapi spec, to be validated against https://editor.swagger.io/ ; spec: http://spec.openapis.org/oas/v3.0.2
* **src**: contains initial Java source code, without business logic. It only contains unfinished code which implements the ER model. Please make sure there are no compile-time errors!

# For developers

* We're doing issue branches: for each issue, make a branch and work on that branch only.
* After your work is done, make a merge request, and add the necessary comments to the MR if necessary. Do not merge! Someone else will do a code review first.